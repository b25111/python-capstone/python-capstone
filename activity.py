from abc import ABC, abstractclassmethod
class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFullName(self):
        fullName = f"{self._firstName} {self._lastName}"
        return fullName

    def addRequest(self):
        return("Request has been added")

    def checkRequest(self):
        pass
    
    def addUser(self):
        return f"User has been added"

    def set_email(self, email):
        self._email = email

    def login(self):
        return(f"{self._email} has logged in")

    def logout(self):
        return(f"{self._email} has logged out")

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._member = []

    def getFullName(self):
        fullName = f"{self._firstName} {self._lastName}"
        return fullName

    def addRequest(self):
        print("Request has been added")

    def checkRequest(self):
        pass

    def addUser(self):
       return f"User has been added"

    def set_email(self, email):
        self._email = email

    def login(self):
        return(f"{self._email} has logged in")

    def logout(self):
        return(f"{self._email} has logged out")

    def addMember(self, employee):
        self._member.append(employee)

    def get_members(self):
        return self._member

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    

    def getFullName(self):
        fullName = f"{self._firstName} {self._lastName}"
        return fullName

    def addRequest(self):
        print("Request has been added")    

    def checkRequest(self):
        pass

    def addUser(self):
        return f"User has been added"

    def set_email(self, email):
        self._email = email

    def login(self):
        return(f"{self._email} has logged in")

    def logout(self):
        return(f"{self._email} has logged out")

    

class Request:
    def __init__(self, name, requester, dateRequested, status ):
        super().__init__()
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = status
   
    
    def updateRequest(self):
        pass
    
    def closeRequest(self):
        pass

    def cancelRequest(self):
        pass

    def set_status(self, status):
        self._status = status

    def set_name(self, name):
        self._name = name
    
    def closeRequest(self):
        return (f"Request {self._name} has been {self._status}")



# Test Cases

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Scepter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021", "open")
req2 = Request("Laptop repair", employee1, "1-Jul-2021", "open")


assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Scepter", "Full name should be Michael Scepter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())

